default: build


build:
	cargo build --release

install_home: build
	cp target/release/arbitrary_pride_flag_printer ~/bin/apfp

