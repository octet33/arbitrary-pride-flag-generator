#![feature(custom_attribute)]

extern crate colored;
use colored::*;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
struct Args {
    #[structopt(short, long)]
    colorstr: Option<String>,

    #[structopt(short, long)]
    flagname: Option<String>,

    #[structopt(short, long)]
    width: Option<usize>,

    #[structopt(short, long)]
    height: Option<usize>,
}

const DEFAULT_WIDTH: usize = 100;
const DEFAULT_HEIGHT: usize = 3;

fn main() {
    let args = Args::from_args();

    //let mut flags = Vec::new();

    // associative array
    // format: [([keys], [values])]
    // a value will be chosen if any key matches
    let flags: &[(&[&str], &[&str])] = &[
        (
            &["enby", "nonbinary"],
            &["yellow", "white", "magenta", "black"],
        ),
        (
            &["trans", "transgender", "default"],
            &["cyan", "magenta", "white", "magenta", "cyan"],
        ),
    ];
    let width = if let Some(w) = args.width {
        w
    } else {
        DEFAULT_WIDTH
    };
    let height = if let Some(h) = args.height {
        h
    } else {
        DEFAULT_HEIGHT
    };

    let parsed_colors = if let Some(ref c) = args.colorstr {
        Option::Some(parse_colors(c.as_ref()))
    } else {
        Option::None
    }; //if we're parsing colors, we need to put them somewhere.

    let colors = if let Some(ref c) = parsed_colors {
        c
    } else {
        lookup_flag_name(
            &flags,
            if let Some(ref f) = args.flagname {
                f
            } else {
                "default"
            },
        )
    };

    print_vert_edge(width);
    print_flag(colors, width, height);
    print_vert_edge(width);
}

fn print_vert_edge(len: usize) {
    for _ in 0..len + 2 {
        print!("{}", " ".on_color("white"));
    }
    println!("");
}

fn lookup_flag_name<'v, 't>(table: &'t [(&[&str], &[&'v str])], name: &str) -> &'t [&'v str] {
    if let Some((_, c)) = table
        .into_iter()
        .find(|(set, _)| set.into_iter().find(|s| **s == name).is_some())
    {
        c
    } else {
        if name == "default" {
            // avoid infinite looping if default is used but not specified
            panic!("default name not found");
        } else {
            lookup_flag_name(table, "default")
        }
    }
}

fn parse_colors(colors: &str) -> Vec<&str> {
    colors.split(",").collect::<Vec<&str>>()
}

fn print_flag(colors: &[&str], width: usize, height: usize) {
    for color in colors.into_iter() {
        for _ in 0..height {
            print_row(color, width);
        }
    }
}

fn print_row(color: &str, width: usize) {
    print!("{}", " ".on_color("white"));
    for _ in 0..width {
        print!("{}", " ".on_color(color));
    }
    println!("{}", " ".on_color("white"));
}
